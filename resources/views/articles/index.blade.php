@extends('app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">
                Articles <small>Overview</small>
            </h1>
            <a style="float: right;position: absolute;top: 0;right: 0;" class="btn btn-primary" href="{{ action('ArticlesController@create') }}">Artikel aanmaken</a>
        </div>
    </div>
    <!-- /.row -->



    @if(count($articles) > 0)
        @foreach($articles as $article)

            <div class="row">
                <div class="col-lg-12 well">
                    <h2 class="page-header">{{ $article->title }}</h2>
                    <p class="lead">{{ $article->excerpt }}</p>
                    <a href="{{ action('ArticlesController@show', [$article->id]) }}">Lees Meer</a>

                    <div style="float:right">
                        <a stlye="float:left;" class="btn btn-default" href="{{ action('ArticlesController@edit', [$article->id]) }}">Edit</a>

                        <form style="float:right; margin-left:5px;" action="{{ URL::route('articles.destroy',$article->id) }}" method="POST">
                            <input type="hidden" name="_method" value="DELETE">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <button class="btn btn-danger">Delete</button>
                        </form>

                    </div>
                </div>
            </div>

        @endforeach
    @else
        <p>Er zijn nog geen artikelen beschikbaar.</p>
    @endif
@stop