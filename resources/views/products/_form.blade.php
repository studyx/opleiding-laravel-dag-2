<div class="form-group">
    {!! Form::label('name', 'Naam product:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('description', 'Beschrijving:') !!}
    {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('amount', 'Bedrag:') !!}
    {!! Form::input('numer', 'amount', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::submit($buttontext, ['class' => 'btn btn-primary form-control']) !!}
</div>