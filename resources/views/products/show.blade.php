@extends('app')

@section('content')
    <h1>{{ $product->title }}</h1>
    <p>{{ $product->body }}</p>
    <p>{{ $product->excerpt }}</p>
    <hr />
@stop