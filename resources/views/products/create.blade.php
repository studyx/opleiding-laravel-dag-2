@extends('app')

@section('content')
    <h1>Product aanmaken</h1>

    {!! Form::open(['url' => 'products']) !!}

        @include('products._form', ['buttontext' => 'Verzenden'])

    {!! Form::close() !!}

@stop