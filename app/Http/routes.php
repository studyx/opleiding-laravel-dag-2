<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Page routes

Route::get('about', ['middleware' => 'auth', 'uses' => 'PagesController@about']);

Route::get('info', 'PagesController@info');

// Articles routes

Route::resource('articles', 'ArticlesController');

// Products routes

Route::resource('products', 'ProductsController');

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);


Route::get('foo', ['middleware' => 'manager', function(){
    return 'this page may only be viewed by managers';
}]);
