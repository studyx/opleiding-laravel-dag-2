<?php

namespace App\Http\Controllers;

use App\Article;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateArticleRequest;
use Illuminate\Support\Facades\Auth;

class ArticlesController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');

        //$this->middleware('auth', ['only' => 'create']);

        $this->middleware('auth', ['except' => 'index']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // return \Auth::user()->name;

        //$articles = Article::all();

        //$articles = Article::latest()->get(); // order by created at

        //$articles = Article::latest('published_at')->get(); // order by published at

        //$articles = Article::latest('published_at')->where('published_at', '<=', Carbon::now())->get();

        $articles = Article::latest('published_at')->published()->get();

        return view('articles.index', compact('articles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        /*if(Auth::guest())
        {
            // redirect
        }*/

        return view('articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateArticleRequest $request)
    {
        //$request = $request->all();
        //$request = $request['user_id'] = Auth::id();

        $article = new Article($request->all());
        //$article->save();

        Auth::User()->articles()->save($article);

        \Session::flash('flash_message', 'Uw artikel is aangemaakt.');

        return redirect('articles');
    }

    /*public function store(Request $request)
    {
        $this->validate($request, ['title' => 'required', 'body' => 'required']);

        $article = new Article($request->all());

        $article->save();

        return redirect('articles');
    }*/

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //dd($id);

        /*$article = Article::find($id); // throw error when article not found

        if(is_null($article)){
            abort(404);
        }*/

        return view('articles.show', compact('article'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $article = Article::findOrFail($id); // throw error when article not found

        return view('articles.edit', compact('article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $article = Article::findOrFail($id);
        $article->update($request->all());

        return redirect('articles/' . $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $article = Article::findOrFail($id);
        $article->destroy($id);

        return redirect('articles');
    }
}
